//
//  common.h
//  BBFMM3D
//
//  Created by Robert Simpson on 24/07/2014.
//  Copyright (c) 2014 Robert Simpson. All rights reserved.
//

#ifndef BBFMM3D_common_h
#define BBFMM3D_common_h

#include <string>
#include <ostream>
#include <utility>

#include <vtkSmartPointer.h>
#include <vtkCellArray.h>

#include "bbfmm.h"

namespace bbfmm3d {
    
    /// Some typedefs to reduce typing effort
    typedef unsigned int uint;
    
    /// Throw an exception with the given error message
    void error(const std::string& errmsg);
    
    /// overload output operator for vector3 type
    std::ostream& operator<<(std::ostream& ost, const vector3& v);
    
    /// get the midpoint of the given vectors for source and field points
    vector3 getCenter(vector3* fieldpts, int nf, vector3* sourcepts, int ns);
    
    /// given a node (i.e. octree cell), recursively populate the cell array with
    /// the appropriate connectivites.
    void addVTKCellConnectivity(nodeT* node, vtkSmartPointer<vtkCellArray> & cellarray);
    
    /// get the set of coordinates for the corner points of a node.
    std::vector<vector3> generateNodeVertices(const vector3 centre, const double L);
    
    /// get the set of coordinates of the mid points of a node. These are the points
    // created when applying octree subdivision.
    std::vector<vector3> generateNodeMidPoints(const vector3 centre, const double L);
    
    /// get the connectivity vector given a local node index, the parent vertex
    /// connectivity, and the connectivity of mid points
    // (generated from subdivision)
    std::vector<size_t> nodeConnectivity(const unsigned int i,
                                         const std::vector<size_t>& parent_conn,
                                         const std::vector<size_t>& midpt_conn);

    /// Transform to appropriate vtk index numbering system
    size_t asVTKVertexI(const size_t index);
    
    /// get the min and max of the given vector of points
    std::pair<vector3, vector3> minmax(vector3* pvec, size_t npoints);
    
    /// Get bounds of given two minmax pairs
    std::pair<vector3, vector3> bounds(std::pair<vector3, vector3> minmax1,
                                       std::pair<vector3, vector3> minmax2);
    
    /// get average of two points
    vector3 average(vector3 p1, vector3 p2);
    
    /// translate a vector of points
    void translate(vector3* pvec, const size_t npoints, vector3 origin);
    
    const double PI = 4.0 * atan(1.0);
}

#endif
