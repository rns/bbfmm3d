    
#include <iostream>
#include <vector>
#include <random>
#include <cassert>
#include <string>

#include "common.h"
#include "bbfmm.h"
#include "bbfmm3d.hpp"

using namespace bbfmm3d;

/// Fill up a vector with range points within the given range
/// The vector should be initialised with the appropriate size
void generate_random_pts(std::vector<vector3>& v,
                         std::pair<double, double>& range)
{
    assert(range.second > range.first);
    std::random_device rd;
    std::mt19937 gen( rd() );
    std::uniform_real_distribution<> dis(range.first, range.second);
    for(auto& p : v) {
        p.x = dis(gen);
        p.y = dis(gen);
        p.z = dis(gen);
    }
}

/// Parameters required for Tree setup
void setMetaData(int& n, doft& dof, int& m, int& level, double& eps) {
    n       = 3;    // Number of Chebyshev nodes per dimension
    dof.f   = 1;//6;
    dof.s   = 1;//9;
    m       = 1;
    level   = 4;
    eps     = 1e-5;
}

int main(int argc, const char * argv[])
{
    if(argc != 4)
        error("Please run as ./intergal_test <number of source points> <number of field points> <filename>\n");
    const unsigned int n_src = std::atoi(argv[1]);
    const unsigned int n_field = std::atoi(argv[2]);
    const std::string filepath(argv[3]);
    
    std::cout << "Creating " << n_src << " source points and " << n_field << " field points\n";
    auto range = std::make_pair(-1.0, 1.0);
    std::vector<vector3> src_pts(n_src);
    generate_random_pts(src_pts, range);
    
    std::vector<vector3> field_pts(n_field);
    generate_random_pts(field_pts, range);
    
    doft dof;
    const double L = range.second - range.first;
    double eps;
    const int use_chebyshev = 1;
    int n, m, level;
    setMetaData(n, dof, m, level, eps);
    std::vector<double> charges(n_src * dof.s * m, 1.0);
        std::vector<double> stress(n_field * dof.f * m);
    std::vector<double> stress_dir(n_field * dof.f * m, 0.0);
    
    const double cps = CLOCKS_PER_SEC;
    const uint n_increments = 1;
    for(uint i = 0; i < n_increments; ++i) {
        const double t0 = clock();
        std::cout << "building octree with " << level + i << " levels       .....\n";
        
        kernel_Laplacian tree(&dof, L, level + i, n, eps, use_chebyshev);
        tree.buildFMMTree();
        DirectCalc3D(&tree, field_pts.data(), n_field, src_pts.data(), charges.data(), m, n_src, &dof,0, L, stress_dir.data());
        std::cout << "finished octree\n";
        const double t1 = clock();
        H2_3D_Compute<kernel_Laplacian> c(&tree, field_pts.data(), src_pts.data(),
                                         n_src, n_field, charges.data(), m, stress.data());
        c.outputVTKOctree(filepath + "octree" + std::to_string(i) + ".vtu");
        const double t2 = clock();
        std::cout << "timings = " << (t1 - t0)/cps << "," << (t2 - t1)/cps << ", total = " << (t2 - t0)/cps << "\n";
        for(unsigned int i = 0; i < n_field; ++i)
            std::cout << stress_dir[i] << " " << stress[i] << "\n";
        stress.clear();
    }
    
    return EXIT_SUCCESS;
}

