#include <stdexcept>
#include <ostream>
#include <limits.h>
#include <vector>
#include <algorithm>
#include <cstddef>

#include <vtkHexahedron.h>
#include "common.h"

namespace bbfmm3d {

    void error(const std::string& errmsg) { throw std::runtime_error(errmsg);}
    
    std::ostream& operator<<(std::ostream& ost, const vector3& v)
    {
        ost << "(" << v.x << "," << v.y << "," << v.z << ")";
        return ost;
    }
    
    vector3 getCenter(vector3* fieldpts, int nf, vector3* sourcepts, int ns)
    {
        const double max_val = std::numeric_limits<double>::max();
        vector3 min; min.x = max_val; min.y = max_val; min.z = max_val;
        vector3 max; max.x = -max_val; max.y = -max_val; max.z = -max_val;
        for(int i = 0; i < nf; ++i) {
            vector3 p = fieldpts[i];
            for(std::size_t c = 0; c < vector3::size(); ++c) {
                const double v = p.get(c);
                if(v < min.get(c)) min.set(c, v);
                if(v > max.get(c)) max.set(c, v);
            }
        }
        
        for(int i = 0; i < ns; ++i) { // now loop over source points
            vector3 p = sourcepts[i];
            for(std::size_t c = 0; c < vector3::size(); ++c) {
                const double v = p.get(c);
                if(v < min.get(c)) min.set(c, v);
                if(v > max.get(c)) max.set(c, v);
            }
        }
        vector3 midpt;
        midpt.x = (max.x + min.x) / 2.0; midpt.y = (max.y + min.y) / 2.0; midpt.z = (max.z + min.z) / 2.0;
        return midpt;
    }
    
    void addVTKCellConnectivity(nodeT* node, vtkSmartPointer<vtkCellArray> & cellarray)
    {
        // only display cells that are non-null and contain
        // non-zero numbers of source points and field points.
        if(!node) return;
        const nodeT* parent_node = node->parent;
        if(parent_node) {
            if(parent_node->Nf == 0 && parent_node->Ns == 0)
                return;
        }
        vtkSmartPointer<vtkHexahedron> hex = vtkSmartPointer<vtkHexahedron>::New();
        for(unsigned int v = 0; v < 8; ++v)
            hex->GetPointIds()->SetId(asVTKVertexI(v), node->connectivity[v]);
        cellarray->InsertNextCell(hex);
        for(unsigned int i = 0; i < 8; ++i) 
            addVTKCellConnectivity(node->leaves[i], cellarray);
        return;
    }
    
    
    std::vector<vector3> generateNodeVertices(const vector3 centre, const double L)
    {
        const unsigned int nnodes = 8;
        const double halfL = L / 2.0;
        std::vector<vector3> nodepts(nnodes,centre);
        for(std::size_t i = 0; i < nnodes; ++i) {
            auto& pt = nodepts[i];
            if(i < 4)
                pt.x -= halfL;
            else
                pt.x += halfL;
            if(i % 4 == 2 || i % 4 == 3)
                pt.y += halfL;
            else pt.y -= halfL;
            if(i % 2 == 0)
                pt.z -= halfL;
            else pt.z += halfL;
        }
        return nodepts;
    }
    
    std::vector<vector3> generateNodeMidPoints(const vector3 centre, const double L)
    {
        const unsigned int nnodes = 19;
        const double halfL = L / 2.0;
        std::vector<vector3> nodepts(nnodes,centre);
        for(int i = 0; i < 15; ++i) {
            auto& pt = nodepts[i];
            if(i < 5)
                pt.x -= halfL;
            if(i > 9)
                pt.x += halfL;
            if(i % 5 == 0)
                pt.y -= halfL;
            if(i % 5 == 2)
                pt.y += halfL;
            if(i % 5 == 1)
                pt.z -= halfL;
            if(i % 5 == 3)
                pt.z += halfL;
        }
        nodepts[15].z -= halfL; nodepts[15].y -= halfL;
        nodepts[16].z -= halfL; nodepts[16].y += halfL;
        nodepts[17].z += halfL; nodepts[17].y += halfL;
        nodepts[18].z += halfL; nodepts[18].y -= halfL;
        return nodepts;
    }
    
    std::vector<size_t> nodeConnectivity(const unsigned int i,
                                         const std::vector<size_t>& pconn,
                                         const std::vector<size_t>& mpconn)
    {
        switch (i) {
            case 0:
                return {pconn.at(0), mpconn.at(0), mpconn.at(1), mpconn.at(4),
                    mpconn.at(15), mpconn.at(5), mpconn.at(6), mpconn.at(9)};
            case 1:
                return {mpconn.at(0), pconn.at(1), mpconn.at(4), mpconn.at(3),
                    mpconn.at(5), mpconn.at(18), mpconn.at(9), mpconn.at(8)};
            case 2:
                return {mpconn.at(1), mpconn.at(4), pconn.at(2), mpconn.at(2),
                    mpconn.at(6), mpconn.at(9), mpconn.at(16), mpconn.at(7)};
            case 3:
                return {mpconn.at(4), mpconn.at(3), mpconn.at(2), pconn.at(3),
                    mpconn.at(9), mpconn.at(8), mpconn.at(7), mpconn.at(17)};
            case 4:
                return {mpconn.at(15), mpconn.at(5), mpconn.at(6), mpconn.at(9),
                    pconn.at(4), mpconn.at(10), mpconn.at(11), mpconn.at(14)};
            case 5:
                return {mpconn.at(5), mpconn.at(18), mpconn.at(9), mpconn.at(8),
                    mpconn.at(10), pconn.at(5), mpconn.at(14), mpconn.at(13)};
            case 6:
                return {mpconn.at(6), mpconn.at(9), mpconn.at(16), mpconn.at(7),
                    mpconn.at(11), mpconn.at(14), pconn.at(6), mpconn.at(12)};
            case 7:
                return {mpconn.at(9), mpconn.at(8), mpconn.at(7), mpconn.at(17),
                    mpconn.at(14), mpconn.at(13), mpconn.at(12), pconn.at(7)};
            default:
                bbfmm3d::error("Bad index supplied for node connectivity.");
                return {};
        }
    }
    
    size_t asVTKVertexI(const size_t index)
    {
        switch (index) {
            case 0: return 0;
            case 1: return 3;
            case 2: return 1;
            case 3: return 2;
            case 4: return 4;
            case 5: return 7;
            case 6: return 5;
            case 7: return 6;
            default:
                error("Bad index supplied");
                return 0;
        }
    }
    
    std::pair<vector3, vector3> minmax(vector3* pvec, size_t npoints)
    {
        const double max = std::numeric_limits<double>::max();
        const double min = -1.0 * max;
        vector3 min_p, max_p;
        for(unsigned int i = 0; i < 3; ++i) {
            min_p.set(i, max);
            max_p.set(i, min);
        }
        for(size_t i = 0; i < npoints; ++i) {
            std::cout << pvec[i] << "\n";
            for(unsigned int c = 0; c < 3; ++c) {
                min_p.set(c, std::min(pvec[i].get(c), min_p.get(c)));
                max_p.set(c, std::max(pvec[i].get(c), max_p.get(c)));
            }
        }
        return std::make_pair(min_p, max_p);
    }
    
    std::pair<vector3, vector3> bounds(std::pair<vector3, vector3> minmax1,
                                       std::pair<vector3, vector3> minmax2)
    {
        vector3 min, max;
        for(unsigned int i = 0; i < 3; ++i) {
            min.set(i, std::min(minmax1.first.get(i), minmax2.first.get(i)));
            max.set(i, std::max(minmax1.second.get(i), minmax2.second.get(i)));
        }
        return std::make_pair(min, max);
    }
    
    vector3 average(vector3 p1, vector3 p2)
    {
        vector3 avg;
        for(unsigned int i = 0; i < 3; ++i)
            avg.set(i, (p1.get(i) + p2.get(i)) / 2.0);
        return avg;
    }
    
    void translate(vector3* pvec, const size_t npoints, vector3 origin)
    {
        for(size_t i = 0; i < npoints; ++i) {
            for(unsigned int c = 0; c < 3; ++c)
                pvec[i].set(c, pvec[i].get(c) - origin.get(c));
        }
    }
}

